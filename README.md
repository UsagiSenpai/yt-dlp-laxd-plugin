# yt-dlp laxd plugin
yt-dlpをLAXD Video `https://video.laxd.com` に対応させるプラグインです。  
淫夢ほんへでしか試していないので有料動画とか高画質とかは知りません。

## インストール

### yt-dlpのダウンロード&インストール
https://github.com/yt-dlp/yt-dlp/releases  
からyt-dlpをダウンロード  
PATHが通る場所に保存  

https://gigazine.net/news/20220101-yt-dlp/  
も参考にしてください

### プラグインのダウンロード&インストール
https://gitlab.com/UsagiSenpai/yt-dlp-laxd-plugin/-/archive/main/yt-dlp-laxd-plugin-main.zip  
からプラグインをダウンロード  
以下のように配置すればOK
```
C:\Users\<USERNAME>\Appdata\Roaming
    ┗📁yt-dlp-plugins
        ┗📁yt-dlp-laxd-plugin
            ┣📄LICENSE      (消してOK)
            ┣📄README.md    (消してOK)
            ┗📁yt_dlp_plugins
                ┗📁extractor
                    ┗📄laxd.py
```