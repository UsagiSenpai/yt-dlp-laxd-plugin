
from yt_dlp.extractor.common import InfoExtractor

class LaxdIE(InfoExtractor):
    _VALID_URL = r'https?://video\.laxd\.com/a/content/(?P<id>[\d\w]+)/?'
    
    def _real_extract(self, url):
        video_id = self._match_id(url)

        # プレイリスト(または動画)のurlを取得
        playlist_json_url = "https://video.laxd.com/api/v3/videoplaylist/" + video_id + "?sh=1&fs=0"
        playlist_data = self._download_json(playlist_json_url, video_id)
        media_url = "https://video.laxd.com" + playlist_data["playlist"]["nq"]

        # メタデータ(ここでは動画のタイトル)を取得
        metadata_json_url = "https://video.laxd.com/api/v3/videoplayer/" + video_id + "?tk=&fs=0"
        metadata = self._download_json(metadata_json_url, video_id)
        title = metadata["title"]

        type = playlist_data["type"]
        if type == 1:
            # ファイルが分割されずにそのまま置かれている事がある
            return {
                "id": video_id,
                "title": title,
                "url": media_url,
                "ext": "mp4"
            }
        else:
            formats = self._extract_m3u8_formats(media_url, video_id, ext="mp4")
            return {
                "id": video_id,
                "title": title,
                "formats": formats
            }